package services;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;
import android.util.Log;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class UploadRouteIntentService extends IntentService {

    public static final String ROUTE_NAME = "route_name";
    public static final String LAT_POSITION = "lat_position";
    public static final String LONG_POSITION = "long_position";
    public  static  final  String TAG_ROUTES = "app_routes";

    DatabaseReference db;

    public UploadRouteIntentService() {
        super("UploadRouteIntentService");
        db = FirebaseDatabase.getInstance().getReference();
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            Log.i(TAG_ROUTES, "INIT");
            String name = intent.getStringExtra(ROUTE_NAME);
            String lat_p = intent.getStringExtra(LAT_POSITION);
            String long_p = intent.getStringExtra(LONG_POSITION);

            DatabaseReference dbTemp = db.child(name).push();
            dbTemp.child(LAT_POSITION).setValue(lat_p);
            dbTemp.child(LONG_POSITION).setValue(long_p);

            try {
                Thread.sleep(20000);
            }catch (InterruptedException ie){
            }

            Log.i(TAG_ROUTES, "FINISH");
        }
    }
}
