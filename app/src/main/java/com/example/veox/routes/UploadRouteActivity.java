package com.example.veox.routes;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import services.UploadRouteIntentService;
import services.UploadRouteService;

public class UploadRouteActivity extends AppCompatActivity {

    Context c;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_route);
        c = this;
    }

    public void upload_route_fb(View v){
        EditText etNameRoute = findViewById(R.id.etNameRoute);
        EditText etLat = findViewById(R.id.etLat);
        EditText etLog = findViewById(R.id.etLong);
        //Intent i = new Intent(c, UploadRouteIntentService.class);
        Intent i = new Intent(c, UploadRouteService.class);
        i.putExtra(UploadRouteIntentService.ROUTE_NAME, etNameRoute.getText().toString());
        i.putExtra(UploadRouteIntentService.LAT_POSITION, etLat.getText().toString());
        i.putExtra(UploadRouteIntentService.LONG_POSITION, etLog.getText().toString());
        startService(i);
    }

}
