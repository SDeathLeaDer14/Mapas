package com.example.veox.routes;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;


public class DownloadRouteActivity extends AppCompatActivity {

    Context c;
    List<String> listNames;
    ArrayAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        c = this;
        setContentView(R.layout.activity_download_route);
        ListView lvRoute = findViewById(R.id.lvRoute);
        downloadData();
        adapter = new ArrayAdapter<String>(c, android.R.layout.simple_list_item_1, listNames);
        lvRoute.setAdapter(adapter);

    }

    public void downloadData(){
        listNames = new ArrayList<>();
        new Thread(new Runnable() {
            @Override
            public void run() {
                DatabaseReference db = FirebaseDatabase.getInstance().getReference();
                db.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        for (DataSnapshot route :dataSnapshot.getChildren()){
                            listNames.add(route.getKey().toString());
                            adapter.notifyDataSetChanged();
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }
        }).start();
    }


}
