package com.example.veox.routes;

import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.android.gms.maps.model.PolylineOptions;

public class MapActivity extends FragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        final SupportMapFragment map = (SupportMapFragment)getSupportFragmentManager().findFragmentById(R.id.fMap);
        final MyMap myMap = new MyMap();
        map.getMapAsync(myMap);

        findViewById(R.id.bDraw).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myMap.googleMap.addPolyline(myMap.route);
            }
        });

        findViewById(R.id.bClean).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myMap.googleMap.clear();
                myMap.route = new PolylineOptions();
            }
        });
    }

    public class MyMap implements OnMapReadyCallback{
        PolylineOptions route = new PolylineOptions();
        GoogleMap googleMap;
        @Override
        public void onMapReady(GoogleMap googleMap) {
            this.googleMap = googleMap;
            googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                @Override
                public void onMapClick(LatLng latLng) {
                    route.add(latLng);
                }
            });
        }
    }
}
