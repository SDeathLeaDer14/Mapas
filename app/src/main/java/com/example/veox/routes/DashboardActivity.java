package com.example.veox.routes;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class DashboardActivity extends AppCompatActivity {

    Context c;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        c = this;

    }

    public void click_upload_route(View v){

        Intent i = new Intent(c, UploadRouteActivity.class);
        startActivity(i);

    }

    public void click_download_route(View v){
        Intent i = new Intent(c, DownloadRouteActivity.class);
        startActivity(i);

    }

    public void click_download_route_map(View v){
        Intent i = new Intent(c, MapActivity.class);
        startActivity(i);
    }
}
